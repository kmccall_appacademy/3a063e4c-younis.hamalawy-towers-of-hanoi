# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers
  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def valid_move?(from_tower, to_tower)
    return false if @towers[from_tower].empty?
     @towers[to_tower].empty? || @towers[from_tower].last < @towers[to_tower].last
  end

  def move(from_tower, to_tower)
    if valid_move?(from_tower, to_tower)
      disk = @towers[from_tower].pop
      @towers[to_tower].push(disk)
    else
      puts "This is not a valid move"
    end
  end

  def won?
    return true if @towers[1].length == 3 || @towers[2].length == 3
    false
  end

  def render
    puts "-_-_-_-_-_-_-_-_-_-_-_-_-_-"
    puts "\n"
    towers_top = @towers.map { |arr| arr.length >= 3 ? " #{'*' * (arr.max - 2)}".ljust(6) : "#{' '}".ljust(6) }
    towers_mid = @towers.map { |arr| arr.length >= 2 ? " #{'*' * (arr.max - 1)}".ljust(6) : "#{' '}".ljust(6) }
    towers_bot = @towers.map { |arr| arr.length >= 1 ? " #{'*' * (arr.max)}".ljust(6) : "#{' '}".ljust(6) }
    puts "#{(towers_top.join'   ').ljust(10)}"
    puts "#{(towers_mid.join'   ').ljust(10)}"
    puts "#{(towers_bot.join'   ').ljust(10)}"
    puts "\n"
    puts " TOWER1   TOWER2   TOWER3 "
    puts "\n"
    puts "-_-_-_-_-_-_-_-_-_-_-_-_-_-"
  end

  def play
    render
    until won?
      puts "Please enter Tower to move from:"
      from_tower = (gets.chomp.to_i - 1)
      if from_tower < 0 || from_tower > 2
        puts "Invalid tower"
      elsif  @towers[from_tower].empty?
        puts "Can't move from an empty tower!"
      else
        puts "Please enter tower to move to:"
        to_tower = (gets.chomp.to_i - 1)
        if to_tower < 0 || to_tower > 2
          puts "Invalid tower"
        else
          move(from_tower, to_tower)
          render
        end
      end
    end
    "CONRATULATIONS! YOU HAVE WON THE GAME!!"
  end
end
